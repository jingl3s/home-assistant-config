import os
import sys
import time

from requests import post

import podfox

# from types import Any


URL = "http://192.168.254.196:8123/api/services/script/1638661080103"
PATH_PODFOX_CONFIG = "~/homeassistant_local/homeassistant/extras/podfox/podfox.json"
DIRECTORY_PODCAST = "les_histoires_holy_owly"
FULL_PATH_PODCAST = f"/home/hugo/homeassistant_local/media/podcasts/{DIRECTORY_PODCAST}"
TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhM2QwOTk1NWU3ZWM0YmI2YjUxNDIyYWZjNzRhYTViNiIsImlhdCI6MTYzODY3NTExMywiZXhwIjoxOTU0MDM1MTEzfQ.YVbJLwi28b0yZr6mc5gVvaDfPmZPhHzJQ3jUIKL6myo"


def call_hass(file_to_play: str, token: str):
    # curl -X POST -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhM2QwOTk1NWU3ZWM0YmI2YjUxNDIyYWZjNzRhYTViNiIsImlhdCI6MTYzODY3NTExMywiZXhwIjoxOTU0MDM1MTEzfQ.YVbJLwi28b0yZr6mc5gVvaDfPmZPhHzJQ3jUIKL6myo" \
    #   -d '{"file_to_play": "pommeapinoel/2021-12-01_1er_décembre_2021_._Le_village_des_Kroons_Ep._1_de_Milo_et_la_bougie_de_Noël.mp3"}' \
    #   -H "Content-Type: application/json" http://192.168.254.196:8123/api/services/script/1638661080103
    headers = {
        "Authorization": f"Bearer {token}",
        "content-type": "application/json",
    }
    payload = {"file_to_play": file_to_play}
    response = post(URL, headers=headers, json=payload)
    if response.status_code != 200:
        print(response.text)


# Python program creating a
# context manager


class ContextManager:
    """
    source:
    https://www.geeksforgeeks.org/__exit__-in-python/
    https://stackoverflow.com/questions/1984325/explaining-pythons-enter-and-exit
    """

    def __init__(self):
        pass

    def __enter__(self):
        pass
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print("Disabled exit called")
        return True

    print("with ContextManager block")


def podfox_update():
    length_sav = len(sys.argv)
    sys.argv += [
        "update",
        DIRECTORY_PODCAST,
        "-c",
        PATH_PODFOX_CONFIG,
    ]
    # print(sys.argv)
    with ContextManager() as _:
        podfox.main()

    del sys.argv[length_sav:]
    print(sys.argv)
    sys.argv += [
        "download",
        DIRECTORY_PODCAST,
        "-c",
        PATH_PODFOX_CONFIG,
        "--rename-files",
    ]
    # print(sys.argv)

    with ContextManager() as _:
        podfox.main()


def get_list_files():
    list_files = os.listdir(FULL_PATH_PODCAST)
    list_files.sort()
    return list_files


def get_last_played():
    if os.path.exists("/mnt/tmpfs/ecoute_podcast.cfg"):
        with open("/mnt/tmpfs/ecoute_podcast.cfg", "r") as input_file:
            lines_text = input_file.readlines()
        return (lines_text[-1][:-1], len(lines_text))
    else:
        return ("", 0)


def save_played(filename: str, new_one=True):
    print(f"Saving {filename} with {new_one}")
    if new_one:
        with open("/mnt/tmpfs/ecoute_podcast.cfg", "w") as output_file:
            output_file.write(f"{filename}\n")
    else:
        with open("/mnt/tmpfs/ecoute_podcast.cfg", "a") as output_file:
            output_file.write(f"{filename}\n")


def main():
    last_played_name, last_played_counter = get_last_played()
    new_file = False
    if "" == last_played_name or 3 == last_played_counter:
        # file_to_play = "pommeapinoel/2021-12-01_1er_décembre_2021_._Le_village_des_Kroons_Ep._1_de_Milo_et_la_bougie_de_Noël.mp3"
        list_fichier = get_list_files()
        # print(list_fichier)
        if 0 == len(list_fichier):
            podfox_update()
            list_fichier = get_list_files()
            # print(list_fichier)
            if 0 == len(list_fichier):
                print("Aucun fichier a jouer")
                sys.exit(0)
        new_file = True
    else:
        list_fichier = [last_played_name]

    print(f"Joue le morceau {DIRECTORY_PODCAST}/{list_fichier[0]}")
    call_hass(f"{DIRECTORY_PODCAST}/{list_fichier[0]}", TOKEN)
    print("Attente fin musique par defaut")
    time.sleep(5 * 60)
    # Remove and update only if it is time to do it
    if 2 == last_played_counter:
        os.remove(f"{FULL_PATH_PODCAST}/{list_fichier[0]}")
        podfox_update()

    save_played(list_fichier[0], new_file)


if __name__ == "__main__":
    main()
