# Home assistant

Configuration home assistant personnel

## Configuration matérielle

<img src="docs/hardware_setup.png" alt="Configuration materiel" width="800"> </img>

## Capture écrans

### Ecran d'information netbook

<img src="images/infos_semaines2.webp" alt="Info semaine 11/2023" width="800"> </img>

<img src="images/infos_semaine.png" alt="Info semaine avec sections dedie" width="800"> </img>

### Ecran d'information netbook

<img src="images/infos_spotify.png" alt="Info netbook" width="800"> </img>

* Cas ci-dessus avec click sur le panier
* Evite d'afficher toujours le widget media player qui consomme du CPU
<img src="images/infos_selection.png" alt="Info netbook" width="800"> </img>

### Ecran d'information en mode netbook et sans menu dedie horaire de bus (pre covid)

 <img src="images/info_panel_current.png" alt="Info panel in progress" width="800"> </img>
 
### Autre examples

<img src="images/info_panel_wifi_on.png" alt="Info panel Wifi on"/>
<br>
<img src="images/info_panel_color_rain.png" alt="Info panel color when rain day" />
<img src="images/info_panel_camera_radar.png" alt="Info panel weather radar" width="800"/>
<img src="images/info_panel_weather_daily.png" alt="Info panel daily weather" width="800"/>
<img src="images/info_panel_weather_hourly.png" alt="Info panel hourly weather" width="800"/>



## Mes configurations detailles

* [Meteo media schools what to wear](docs/meteo_media_school_wear.md)
* [Spotify daemon player with media player](docs/spotify_daemon.md)
* [Musique le matin via Spotify ou VLC](docs/musique_matin.md)
* [Ecoute des podcast](docs/ecoute_podcast.md)
* [Action tous les jours](docs/action_journaliere.md)

### Autre réglages

#### Mes demarrage sans service

/home/hugo/.config/openbox/autostart

#bash /home/hugo/homeassistant_local/homeassistant/bin/launch.sh &

#### Service linux

* Service lancé au démarrage<br> 
[https://community.home-assistant.io/t/autostart-using-systemd/199497](https://community.home-assistant.io/t/autostart-using-systemd/199497)

```shell
sudo nano /lib/systemd/system/homeassistant@USER.service
sudo nano /lib/systemd/system/homeassistant@hugo.service
```
USER est remplacé par le compte utilisateur

```ini
[Unit]
Description=Home Assistant
After=network-online.target

[Service]
Type=simple
User=%i
WorkingDirectory=/home/%i/homeassistant_local/homeassistant
Environment="LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2"
Environment=MY_HOMEASSISTANT_PATH=/home/%i/homeassistant_local/homeassistant
Environment=MY_HOMEASSISTANT_PYTHON=/home/%i/homeassistant_local/venv/bin/python>
# ExecStart=$MY_HOMEASSISTANT_PYTHON /home/hugo/homeassistant_local/venv/bin/hass -c $MY_HOMEASSISTANT_PATH
ExecStart=/home/%i/homeassistant_local/venv/bin/python3 /home/%i/homeassistan>
#ExecStart=/home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassi>
#ExecStart=/home/%i/homeassistant_local/venv310_malloc/bin/python /home/%i/ho>
#ExecStart=/bin/bash /home/%i/homeassistant_local/homeassistant/bin/start_has>
RestartForceExitStatus=100
# Restart=on-failure
# RestartSec=5s

[Install]
WantedBy=multi-user.target
``` 


sudo systemctl --system daemon-reload

To have Home Assistant start automatically at boot, enable the service.

sudo systemctl enable home-assistant@YOUR_USER

To disable the automatic start, use this command.

sudo systemctl disable home-assistant@YOUR_USER

To start Home Assistant now, use this command.

sudo systemctl start home-assistant@YOUR_USER


sudo systemctl status home-assistant@YOUR_USER

## Notes

* Mode remote debug
  * sur pc development et non sur cible
    * avoir un fichier configuration.yaml allégé mais avec 
      * auth_providers
      * frontend
      * lovelace
      * sensor sur le sensor utilisé pour le débug
    * Suivre la procedure depuis [https://www.home-assistant.io/integrations/debugpy/](https://www.home-assistant.io/integrations/debugpy/)
    * Il n'est pas nécéssaire d'avoir tout le code home assistant si debug d'un composant perso sinon un peu plus délicas
  * Lancer home assistant `hass -c .`

* Styles disponibles pour les cartes lovelace
  * [https://github.com/home-assistant/home-assistant-polymer/blob/master/src/resources/ha-style.ts](https://github.com/home-assistant/home-assistant-polymer/blob/master/src/resources/ha-style.ts)

* Couleurs CSS
  * [https://www.w3schools.com/cssref/css_colors.asp](https://www.w3schools.com/cssref/css_colors.asp)


* Tableau de bord Android https://wallpanel.app/

## Reglages

### Mode kiosk pour reduire l'empreinte du menu
* [https://gist.github.com/ciotlosm/1f09b330aa5bd5ea87b59f33609cc931](https://gist.github.com/ciotlosm/1f09b330aa5bd5ea87b59f33609cc931)
* [https://gist.github.com/Zemoj/d2893f6be8ac5b7c58541ff92eeea54b](https://gist.github.com/Zemoj/d2893f6be8ac5b7c58541ff92eeea54b)

## A essayer

* Cards a ajouter
  * https://github.com/zsarnett/slideshow-card

* Changer affichage bus avec des bulles pour voir si reduction taille
  * [https://jsfiddle.net/6cygbd37/1/](https://jsfiddle.net/6cygbd37/1/)
  * [https://www.w3schools.com/w3css/w3css_round.asp](https://www.w3schools.com/w3css/w3css_round.asp)

* Changer le message en surimpression en cas de deconnexion
  * [https://gist.github.com/thomasloven/5ee36708908569c8e168419557cefd08](https://gist.github.com/thomasloven/5ee36708908569c8e168419557cefd08)
  * [https://github.com/home-assistant/frontend/blob/7b0fb949fd53f5f85a38ef80420bd911a97fbd82/src/components/ha-toast.ts#L10-L20](https://github.com/home-assistant/frontend/blob/7b0fb949fd53f5f85a38ef80420bd911a97fbd82/src/components/ha-toast.ts#L10-L20)
  * 
* Changement dynamique de la mise a jour d'une entité
  * Util pour un composant qui doit evoluer
  * Utilisation de scripts tel que trouvé https://community.home-assistant.io/t/change-scan-interval-for-image-processing-from-script-automation/148478
* Composant pour faire des interraction web avec des dependances fournies par Home assistant
  * https://github.com/home-assistant/core/blob/7ea8d72133e7fdab9f98e5e7051ea71da8bfe88e/homeassistant/components/scrape/sensor.py#L7
