# -*- coding: latin-1 -*-
'''
@author: 2020 jingl3s at yopmail dot com
'''

# license
# 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import requests
import logging
import json
from requests import post, get

class HomeAssistant(object):
    '''
    Permet d'envoyer des commandes au serveur Home assistant

    https://developers.home-assistant.io/docs/api/rest/#post-apistatesentity_id
    '''
# curl -X GET -H "Authorization: Bearer TBD" -H "Content-Type: application/json" http://192.168.254.196:8123/api/states/sensor.lufa_status
# curl -X POST -H "Authorization: Bearer TBD" -H "Content-Type: application/json" -d '{"state": "25"}' http://192.168.254.196:8123/api/states/sensor.lufa_status
    URL_HASS = "{0}/api/states/sensor.{1}"
    URL_HASS_SERVICE = "{0}/api/services/input_select/set_options"

    def __init__(self):
        '''

        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._adresse = None
        self._url_lit = None
        self._requete_val = None
        self._sensor_name = ''
        self._sensor_name = ''
        self._url = ''
        self._headers = None

    def set_adresse(self, adresse):
        self._adresse = adresse
        self._url = self.URL_HASS.format(self._adresse, self._sensor_name)

    def set_sensor_name(self, sensor):
        self._sensor_name = sensor
        self._url = self.URL_HASS.format(self._adresse, self._sensor_name)

    def set_input_select_name(self, sensor):
        self._sensor_name = sensor
        self._url = self.URL_HASS_SERVICE.format(self._adresse, self._sensor_name)

    def set_url_lecture(self, url_lecture):
        self._url_lit = url_lecture

    def set_token(self, token):
        self._headers = {
            "Authorization": f"Bearer {token}",
            "content-type": "application/json",
        }

    def set_input_select(self, state_value):
        payload = {'entity_id': self._sensor_name, 'options': state_value}
        response = post(self._url, headers=self._headers, json=payload)
        if response.status_code != 200:
            self._logger.error(response.text)

    def get_valeur(self):
        response = get(self._url, headers=self._headers)
        return response.text
