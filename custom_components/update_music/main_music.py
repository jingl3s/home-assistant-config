# -*- coding: latin-1 -*-
"""
@author: zorbac

"""
# license
#
# Copyright zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import os
import socket
import sys
import glob

# from common.configuration_loader import ConfigurationLoader
# from common.logger_config import LoggerConfig
import datetime

# from lufa_crawl import Lufa
import hass
import cred

# def configure():
#     """
#     :param config_select:

#     :return: tuple avec en premier un logger configuré et en second la configuration json lues

#     """
#     global _logger
#     # LOGGER
#     my_logger = LoggerConfig(
#         os.path.join(os.path.dirname(__file__), "log"), "lanceur", log_level
#     )
#     _logger = my_logger.get_logger()

#     _logger.info(f"LOGGER activated at level {log_level}")

#     return _logger, _config_json, dossier_config


def get_list_files(p_path):
    """
    Generate an output list of files with relative path from input as used for processing
    """
    #
    save_path = os.getcwd()
    os.chdir(p_path)

    # list_content = os.listdir(p_path)

    # list_music = [ f"- '{filename}'"  for filename in list_content if filename.endswith('.mp3')]

    list_content = glob.glob(f"**/*.mp3", recursive=True)

    # remove full path

    # list_content2 = [filename.replace(p_path, '') for filename in list_content ]
    # list_music = [ f"- \"{filename}\""  for filename in list_content]
    # list_music = [ f"\"{filename}\""  for filename in list_content]
    list_music = list_content

    output_text = "\r\n".join(list_music)
    # print(output_text)

    os.chdir(save_path)

    pass

    return list_music


def main():
    global hass_obj

    # test_dir = "C:/Users/T932215/Music"
    test_dir = "/home/hugo/media/musique/"

    hass_obj = hass.HomeAssistant()
    hass_obj.set_adresse("http://192.168.254.196:8123")
    hass_obj.set_token(cred.hass_token)
    hass_obj.set_input_select_name("input_select.media_musiques")

    list_files = get_list_files(test_dir)
    hass_obj.set_input_select(list_files)


if __name__ == "__main__":
    main()
