# État patinoire Montreal

## Objectif
* Obtenir l'état de la patinoire de Montreal
* Integration le plus simple

## Fichiers

* patinoire/const.py
* patinoire/dev_exec.py : Script permet de faire des essais sur le composant de base
* patinoire/manifest.json
* patinoire/patinoire_r_d.ipynb : Jupyter notebook pour la mise au point rapide
* patinoire/sensor_alone.py : Sensor détaché de l'intégration avec Home assistant
* patinoire/sensor.py: Sensor intégré dans HASS

## Sources de données

http://ville.montreal.qc.ca/portal/page?_pageid=5798,94909650&_dad=portal&_schema=PORTAL#cdn
  La ville de montreal fourni les données
  http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml

https://www.patinermontreal.ca 
Celui-ci permet l'accès à leur données

## Recherches

* Utilisation du composant HASS scrape qui n'a pas fonctionné
```yaml
platform: scrape
resource: http://ville.montreal.qc.ca/portal/page?_pageid=5798,94909650&_dad=portal&_schema=PORTAL#cdn
select: "div.tab_csh:nth-child(4) > table:nth-child(4) tr:nth-child(5)"
scan_interval: 7200
name: patinoire
value_template: >-
    {{value|truncate(254,True)}}
```

* Utilisation du composant HASS rest qui n'a pas fonctionné
https://www.home-assistant.io/integrations/sensor.rest/
https://community.home-assistant.io/t/restful-and-xml-integration/179202/22

```yaml
platform: rest
resource: https://www.patinermontreal.ca/data.json
# resource: http://192.168.254.199:8000/data.json
name: patinoire
json_attributes_path: "$.*.patinoires[?(@.id == 549)].nom"
value_template: >-
    {{value|truncate(254,True)}}
```

* Recherche recupération depuis JSON

https://jsonpath.com/
https://www.patinermontreal.ca/data.json
https://www.patinermontreal.ca/patinoires/549-jean-brillant
https://goessner.net/articles/JsonPath/
https://jsonpath.curiousconcept.com/#

Site Json path tests
`$.*.*[?(@.cle='cdn')].`
`$.*.[?(@.cle == 'cdn' && @.id == 549)]`
`$.*.patinoires[?(@.id == 549)].condition`

Utilisation de jq
`cat data.json |jq --jsonargs "$.*.*[?(@.cle='cdn')]."`

https://gist.github.com/ipbastola/2c955d8bf2e96f9b1077b15f995bdae3

`cat data.json | jq -c '.[] | select(.cle == "cdn")' |head`
`cat data.json | jq -c '.[].patinoires' `
`cat data.json | jq -c '.[].patinoires[0]id'`
`cat data.json | jq -c '.[].patinoires[] | select (.id == 549)'`

* Sorties utilisées

# "id": 549,
# "nom": "Aire de patinage libre, Jean-Brillant (PPL)",
# "description": "Aire de patinage libre",
# "genre": "PPL",
# "ouvert": false,
# "deblaye": false,
# "arrose": false,
# "resurface": false,
# "condition": "Mauvaise",
# "parc": "Jean-Brillant",
# "adresse": "5252, avenue Decelles",
# "tel": null,
# "ext": null,
# "lat": 45.496913,
# "lng": -73.619434,
# "slug": "jean-brillant"

* La solution retenue et l'utilisation du fichier de données ouvertes de Montreal
http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml
