"""
Connect to montreal patinoire states as html and grab the state for a hard coded patinoire

Based on my meteo_media_pollen custom components
"""
import logging
from datetime import timedelta
from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle

from .sensor_alone import PatinoireDataBase

COMPONENT_NAME = "patinoire"

SCAN_INTERVAL = timedelta(seconds=7200)

_LOGGER = logging.getLogger(__name__)


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""

    data_retriever = PatinoireData(
        interval=SCAN_INTERVAL,
        url="http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml",
    )
    # Force update as nothing happen otherwise
    data_retriever.update()

    list_sensor = list()
    list_sensor.append(PatinoireSensor(hass, data=data_retriever))
    # Force update
    list_sensor[-1].update()

    add_devices(list_sensor)


class PatinoireSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self, hass, data):
        """Initialize the sensor."""
        super(PatinoireSensor, self).__init__()
        self._state = None
        self._hass = hass
        self._LOGGER = logging.getLogger(__name__)
        self._name = COMPONENT_NAME
        self._icon = "mdi:skate"
        self._data = data
        self._attributes = dict()

        # Id will be unique as name will contain a counter used on init
        self.entity_id = f"sensor.{COMPONENT_NAME}"

    def __str__(self):
        return f"name: {self._name}"

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return None

    @property
    def extra_state_attributes(self):
        """Return the attribute of the sensor."""
        return self._attributes

    @property
    def icon(self):
        """Icon to use in the frontend, if any."""
        return self._icon

    @Throttle(SCAN_INTERVAL)
    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        self._LOGGER.debug("==Update")

        self._data.update()

        element = self._data.get_data()
        self._LOGGER.debug(element)
        self._state = element
        # self._attr_available = element != ""
        self._attributes = self._data.get_data_attributes()
        self._LOGGER.debug(self._attributes)

    @property
    def unique_id(self):
        return self.entity_id


class PatinoireData(PatinoireDataBase):
    """Get the data from external sensor."""

    def __init__(self, interval, url):
        """Initialize the data object."""
        super(PatinoireDataBase, self).__init__()
        PatinoireDataBase.__init__(self, interval, url)

        # Apply throttling to methods using configured interval
        self.update = Throttle(interval)(self._update)
