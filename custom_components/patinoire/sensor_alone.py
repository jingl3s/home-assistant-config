import logging
import os
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import List

import requests
from bs4 import BeautifulSoup


class data_type(Enum):
    day = 0
    detail = 1


@dataclass(order=True)
class PollenTree:
    """Class for Tree Pollen"""

    level: str
    name: str


class PatinoireDataBase:
    """Get the data from external sensor."""

    def __init__(self, interval, url):
        """Initialize the data object."""
        self._data: str = ""
        self._data_detail = dict()

        self._logger = logging.getLogger(__name__)

        self._url_site = url

        self._web_page_content = None
        # Header with personal information as idea from
        # http://robertorocha.info/on-the-ethics-of-web-scraping/
        self._header = {
            "user-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0;Hugo Carnide/Montreal/jingl3s@yopmail.com",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "accept-Language": "fr",
            "accept-encoding": "gzip, deflate",
            "connection": "keep-alive",
            "pragma": "no-cache",
            "cache-control": "no-cache",
        }

    def _update(self):
        """Get the latest data."""
        self._retrieve_content()

    def get_data(self) -> str:
        self._logger.debug(self._data)
        return self._data

    def get_data_attributes(self) -> dict:
        if self._data_detail is not None:
            return self._data_detail
        
        self._logger.warn("No state identified")
        return {}

    def _retrieve_content(self):
        self._logger.debug("Recuperation informations")

        if os.path.isfile(self._url_site):
            with open(self._url_site) as red:
                page_content = red.read()
        else:
            page_content = requests.get(self._url_site, headers=self._header).text

        self._extract_content(page_content)

    def _extract_content(self, page_content):
        soup = BeautifulSoup(page_content, features="xml")

        # print(soup.find_all('nom')[0:5])
        # soup.find_all('nom', string=re.compile("Jean-Brillant (PPL)"))
        val = soup.find_all(string="Aire de patinage libre, Jean-Brillant (PPL)")
        if not val:
            self._data = ""
            self._data_detail = {}
            return 
     
        name_patinoire = val[0].text
        patinoire_node = val[0].parent.parent

        # Recuperation date mise a jour
        date = patinoire_node.arrondissement.date_maj
        dt_format = datetime.strptime(date.text, "%Y-%m-%d %H:%M:%S")
        date_formatted = dt_format.strftime("%d/%m %Hh")

        # Recuperation informations de la patinoire
        states = dict()
        states["ouvert"] = patinoire_node.ouvert.text
        states["deblaye"] = patinoire_node.deblaye.text
        states["arrose"] = patinoire_node.arrose.text
        states["resurface"] = patinoire_node.resurface.text
        states["condition"] = patinoire_node.condition.text

        # Format data
        if "Jean-Brillant (PPL)" in name_patinoire:
            name_patinoire = "Jean-Brillant"

        sensor_state = name_patinoire

        # value_template: '{{ value | replace (",", ".") | float }}'
        #  tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(1)
        patinoire_state = states["condition"]
        if patinoire_state == "N/A":
            self._data = ""
            self._data_detail = {}
            return

        sensor_state += f" : {patinoire_state}"
        if patinoire_state == "Bonne":
            for detail in ["ouvert", "deblaye", "arrose", "resurface"]:
                if states[detail] == "":
                    sensor_state += f", {detail}:non"

        sensor_state += f" {date_formatted}"

        if 255 < len(sensor_state):
            sensor_state = sensor_state[:254]

        self._data = sensor_state
        self._logger.debug(self._data)

        self._data_detail = dict()
        for key, value in states.items():
            if value == "":
                self._data_detail[key] = "0"
            else:
                self._data_detail[key] = value

        self._logger.debug(self._data_detail)
