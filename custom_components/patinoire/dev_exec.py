import logging

from sensor_alone import PatinoireDataBase


def get_data(data_retriever):
    """Fetch new state data for the sensor.

    This is the only method that should fetch new data for Home Assistant.
    """
    element = data_retriever.get_data()

    if element is not None:
        if "Pas de rapport" not in element:
            state = element
        else:
            state = None
    else:
        state = None

    print(state)
    attributes = data_retriever.get_data_attributes()
    print(attributes)


def do_stuff():

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s-%(levelname)7s-[%(name)s.%(funcName)s]-%(message)s"
    )
    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    data_retriever = PatinoireDataBase(
        interval=5,
        url="http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml",
        # url="/mnt/1T/01_Documents_Secondaires/Programmation/GitLab_Projets/home-assistant-config/custom_components/patinoire/L29_PATINOIRE.xml",
    )
    # Force update as nothing happen otherwise
    data_retriever._update()

    get_data(data_retriever)


if __name__ == "__main__":
    do_stuff()
