import logging

from sensor_alone import MeteoMediaDataBase, data_type


def get_data(data_retriever):
    """Fetch new state data for the sensor.

    This is the only method that should fetch new data for Home Assistant.
    """
    index = 0
    element = data_retriever.get_data(index)

    if element is not None:
        if "Pas de rapport" not in element:
            state = element
        else:
            state = None
    else:
        state = None

    attributes = data_retriever.get_data_day_details_attributes(index)
    print(attributes)


def do_stuff():

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s-%(levelname)7s-[%(name)s.%(funcName)s]-%(message)s"
    )
    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    data_retriever = MeteoMediaDataBase(
        interval=5, url="https://www.meteomedia.com/ca/plein-air/pollen/quebec/montreal"
    )
    # Force update as nothing happen otherwise
    data_retriever._update()

    get_data(data_retriever)


if __name__ == "__main__":
    do_stuff()
