"""
Connect to meteomedia as html and grab the pollen level


Sources to help this
https://github.com/home-assistant/example-custom-config
https://github.com/home-assistant/example-custom-config/blob/master/custom_components/hello_world.py
https://github.com/home-assistant/home-assistant/blob/dev/homeassistant/helpers/entity_component.py
https://github.com/home-assistant/home-assistant/blob/dev/homeassistant/helpers/entity.py

https://dev-docs.home-assistant.io/en/master/api/core.html#homeassistant.core.StateMachine
https://developers.home-assistant.io/docs/en/creating_platform_index.html

Manifest file
https://developers.home-assistant.io/docs/en/creating_integration_manifest.html

"""
import datetime
import json
import logging
import os
from datetime import timedelta

import homeassistant.helpers.config_validation as cv
import requests
import voluptuous as vol
from bs4 import BeautifulSoup
from homeassistant.components.weather import ATTR_FORECAST_CONDITION
from homeassistant.exceptions import PlatformNotReady
from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle
from requests import session

from .sensor_alone import CONDITION_PICTURES as CONDITION_PICTURES
from .sensor_alone import MeteoMediaDataBase, data_type

COMPONENT_NAME = "meteo_media_pollen"

SCAN_INTERVAL = timedelta(seconds=7200)
# SCAN_INTERVAL = timedelta(seconds=900)

PICTURE_PATH = f"/local/{COMPONENT_NAME}/pollen-dust"
PICTURE_EXT = ".svg"
DEFAULT_PICTURE = f"{PICTURE_PATH}{PICTURE_EXT}"

_LOGGER = logging.getLogger(__name__)


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""

    # Check images are installed
    default_picture = DEFAULT_PICTURE
    default_picture2 = default_picture.replace("/local", "www")
    current_dir = os.path.dirname(__file__)
    default_picture_path = os.path.join(current_dir, "..", "..", default_picture2)
    if not os.path.exists(default_picture_path):
        # _LOGGER.debug(f"Path of file: {__file__}")
        _LOGGER.error(
            f"Pictures not installed in: {default_picture_path} from {__file__} dir {current_dir}"
        )

    data_retriever = MeteoMediaData(
        interval=SCAN_INTERVAL,
        url="https://www.meteomedia.com/ca/plein-air/pollen/quebec/montreal",
    )
    # Force update as nothing happen otherwise
    data_retriever.update()

    list_sensor = list()
    for count in range(3):
        list_sensor.append(
            MeteoMediaFormatSensor(hass, index=count, data=data_retriever)
        )
        # Force update
        list_sensor[-1].update()

    # Sensor detail jour en cours
    mm_detail0 = MeteoMediaFormatSensorDetail(hass, index=0, parent=list_sensor[0])
    list_sensor.append(mm_detail0)
    list_sensor[-1].update()

    add_devices(list_sensor)


class MeteoMediaFormatSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self, hass, index, data):
        """Initialize the sensor."""
        super(MeteoMediaFormatSensor, self).__init__()
        self._state = None
        self._hass = hass
        self._LOGGER = logging.getLogger(__name__)
        self._name = ("{}_day{}".format(COMPONENT_NAME, index),)
        self._index = index
        self._icon = "mdi:air-filter"
        self._data = data
        self._attributes = dict()
        self._condition_picture = dict(CONDITION_PICTURES)

        # Id will be unique as name will contain a counter used on init
        self.entity_id = "{}.{}_day_{}".format("sensor", COMPONENT_NAME, index)

    def __str__(self):
        return "name: {}, index: {}".format(self._name, self._index)

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return None

    @property
    def extra_state_attributes(self):
        """Return the attribute of the sensor."""
        return self._attributes

    @property
    def entity_picture(self):
        """Return the entity picture to use in the frontend, if any."""
        if self._state in self._condition_picture:
            return (
                f"{PICTURE_PATH}-{self._condition_picture[self._state][0]}{PICTURE_EXT}"
            )
        return DEFAULT_PICTURE

    @property
    def icon(self):
        """Icon to use in the frontend, if any."""
        return self._icon

    @Throttle(SCAN_INTERVAL)
    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        self._LOGGER.debug("==Update: {}".format(self._index))

        self._data.update()

        element = self._data.get_data(self._index)

        if element is not None:
            if "Pas de rapport" not in element:
                self._state = element
            else:
                self._state = None
        else:
            # self._LOGGER.warn("No state identified")
            self._state = None

        self._attributes = self._data.get_data_day_details_attributes(self._index)

    @property
    def unique_id(self):
        return self.entity_id


class MeteoMediaFormatSensorDetail(Entity):
    """Representation of a Sensor."""

    def __init__(self, hass, index, parent: MeteoMediaFormatSensor):
        """Initialize the sensor."""
        super(MeteoMediaFormatSensorDetail, self).__init__()
        self._state = None
        self._hass = hass
        self._LOGGER = logging.getLogger(__name__)
        self._name = ("{}_day{}_detail".format(COMPONENT_NAME, index),)
        self._index = index
        self._icon = "mdi:air-filter"
        self._condition_picture = dict(CONDITION_PICTURES)
        self._parent = parent
        self._LOGGER.debug(type(self._parent))
        # Id will be unique as name will contain a counter used on init
        self.entity_id = "{}.{}_day_{}_detail".format("sensor", COMPONENT_NAME, index)

    def __str__(self):
        return "name: {}, index: {}".format(self._name, self._index)

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return None

    @property
    def device_state_attributes(self):
        """Return the attribute of the sensor."""
        return None

    @property
    def entity_picture(self):
        """Return the entity picture to use in the frontend, if any."""
        # return self._picture
        return None

    @property
    def icon(self):
        """Icon to use in the frontend, if any."""
        return self._icon

    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        self._LOGGER.debug("==Update: {}".format(self._index))
        if self._parent is None:
            return ""
        self._LOGGER.debug(type(self._parent))
        self._LOGGER.debug(self._parent)
        attributes = self._parent._attributes

        self._state = ""
        level = ""
        tree = ""
        for attribue in attributes.items():
            if "tree" in attribue[0]:
                tree = attribue[1]
            elif "level" in attribue[0]:
                level = attribue[1]
            elif tree != "" and level != "":
                self._state += f"{tree},{level},"
                level = ""
                tree = ""
        if 255 >= len(self._state):
            self._state = self._state[:255]


class MeteoMediaData(MeteoMediaDataBase):
    """Get the data from external sensor."""

    def __init__(self, interval, url):
        """Initialize the data object."""
        super(MeteoMediaDataBase, self).__init__()
        # super(MeteoMediaDataBase, self).__init__(interval, url)
        MeteoMediaDataBase.__init__(self, interval, url)

        # Apply throttling to methods using configured interval
        self.update = Throttle(interval)(self._update)
