import logging
import os
from dataclasses import dataclass
from enum import Enum

import requests
from bs4 import BeautifulSoup


class data_type(Enum):
    day = 0
    day_detail = 1


# Add this to retrieve the correct image
CONDITION_PICTURES = {
    "Très élevé": ["red", "red"],
    "Élevé": ["red", "red"],
    "Modéré": ["jaune", "yellow"],
    "Bas": ["green", "green"],
    "Faible": ["green", "green"],
    "default": ["grey", "grey"],
}


@dataclass(order=True)
class PollenTree:
    """Class for Tree Pollen"""

    level: str
    name: str


class MeteoMediaDataBase:
    """Get the data from external sensor."""

    def __init__(self, interval, url):
        """Initialize the data object."""
        self.data = list()
        self._data_detail = list()

        self._logger = logging.getLogger(__name__)

        self._url_site = url

        self._web_page_content = None
        # Header with personal information as idea from
        # http://robertorocha.info/on-the-ethics-of-web-scraping/
        self._header = {
            "user-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0;Hugo Carnide/Montreal/jingl3s@yopmail.com",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "accept-Language": "fr",
            "accept-encoding": "gzip, deflate",
            "connection": "keep-alive",
            "pragma": "no-cache",
            "cache-control": "no-cache",
        }
        self._count_issues: int = 0

    def _update(self):
        """Get the latest data."""
        self._retrieve_content()

    def get_data(self, index, type=data_type.day):
        if type == data_type.day:
            if 0 != len(self.data) and index < len(self.data):
                return self.data[index]
        elif type == data_type.day_detail:
            if 0 != len(self._data_detail) and index < len(self._data_detail):
                return self._data_detail[index]
        return None

    def get_data_day_details_attributes(self, index) -> dict:
        element = self.get_data(index, data_type.day_detail)
        attributes = dict()
        if element is not None:
            dict_attributes = dict()
            for index, pollen_tree in enumerate(element):
                intermediate = pollen_tree.name
                if "-" in intermediate:
                  current_pollen_tree = intermediate.split("-")[-1].strip()
                else:
                  current_pollen_tree = intermediate
                current_pollen_level = pollen_tree.level
                if pollen_tree.level in CONDITION_PICTURES:
                    current_pollen_color = CONDITION_PICTURES[pollen_tree.level][1]
                else:
                    current_pollen_color = CONDITION_PICTURES["default"][1]
                dict_attributes[current_pollen_tree] = [
                    current_pollen_level,
                    current_pollen_color,
                ]

            index = 0
            for tree, tree_detail in sorted(dict_attributes.items()):
                attributes[f"detail{index}_tree"] = tree
                attributes[f"detail{index}_level"] = tree_detail[0]
                attributes[f"detail{index}_color"] = tree_detail[1]
                index += 1
            # Add missing inputs to values
            for cpt in range(index, 3):
                attributes[f"detail{cpt}_tree"] = ""
                attributes[f"detail{cpt}_level"] = ""
                attributes[f"detail{cpt}_color"] = ""
        else:
            # LOGGER.warn("No state identified")
            attributes["detail"] = None
        return attributes

    def _retrieve_content(self):

        self._logger.debug("Recuperation informations")

        if os.path.isfile(self._url_site):
            with open(self._url_site) as red:
                page_content = red.read()
        else:
            page_content = requests.get(self._url_site, headers=self._header).text

        self._extract_content(page_content)

    def _extract_content(self, page_content):
        soup = BeautifulSoup(page_content, "html.parser")

        previsions_pollen = soup.find_all(
            "div", attrs={"data-testid": "forecast-period"}
        )

        if 0 == len(previsions_pollen):
            # Ignore cas as no output from request
            self._logger.debug("Unable to find pollens in meteo media output")
            if self._count_issues == 3:
                del self.data[:]
                self.data = [
                    None,
                ] * 3

                del self._data_detail[:]
                self._data_detail = [
                    list(),
                ] * 3
            else:
                self._count_issues += 1
        else:
            number_days = len(previsions_pollen)
            if len(self.data) < number_days:
                del self.data[:]
                self.data = [
                    "",
                ] * number_days
            if len(self._data_detail) != number_days:
                del self._data_detail[:]
                self._data_detail = [
                    list(),
                ] * number_days

            for current_day, pollen_day in enumerate(previsions_pollen):
                pollen_day_div = pollen_day.find_all("div", recursive=False)

                pollen_day_overall = pollen_day_div[1].span.text
                self.data[current_day] = pollen_day_overall

                pollen_day_details = pollen_day_div[1].find_all("div", recursive=False)
                self._data_detail[current_day] = list()

                for index, pollen_day_detail in enumerate(pollen_day_details):
                    level = pollen_day_detail.div.span.text

                    p_tree = PollenTree(
                        level=level,
                        name=pollen_day_detail.find_all("div", recursive=False)[1].text,
                    )
                    self._data_detail[current_day].append(p_tree)
            self._logger.debug(self._data_detail)
            self._count_issues = 0
