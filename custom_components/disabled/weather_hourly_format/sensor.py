"""

Sources to help this
https://github.com/home-assistant/example-custom-config
https://github.com/home-assistant/example-custom-config/blob/master/custom_components/hello_world.py

https://github.com/home-assistant/home-assistant/tree/dev/homeassistant/components/darksky
https://github.com/home-assistant/home-assistant/blob/dev/homeassistant/helpers/entity_component.py
https://github.com/home-assistant/home-assistant/blob/dev/homeassistant/helpers/entity.py

https://github.com/cmoore42/home-assistant/blob/wunderground-forecast/homeassistant/components/sensor/wunderground.py

https://dev-docs.home-assistant.io/en/master/api/core.html#homeassistant.core.StateMachine
https://developers.home-assistant.io/docs/en/creating_platform_index.html

Manifest file
https://developers.home-assistant.io/docs/en/creating_integration_manifest.html

Use of component homeassistant.components.darksky indirectly
"""
import voluptuous as vol
from homeassistant.const import DEGREE
from homeassistant.helpers.entity import Entity
from homeassistant.exceptions import PlatformNotReady
from homeassistant.components.weather import ATTR_FORECAST_CONDITION
from homeassistant.util import Throttle
import homeassistant.helpers.config_validation as cv

# from homeassistant.components.darksky.sensor import CONDITION_PICTURES
from homeassistant.util import dt
from homeassistant.components.sensor import PLATFORM_SCHEMA

import logging
import datetime
from datetime import timedelta

CONDITION_PICTURES = dict()
COMPONENT_NAME = "weather_hourly_format"

# SCAN_INTERVAL = timedelta(seconds=3600)
SCAN_INTERVAL = timedelta(seconds=1800)

CONF_WEATHER_SENSOR = "weather_sensor"
CONF_WEATHER_HOUR_BETWEEN = "hour_between"
CONF_LABEL_SENSOR = "label"

_LOGGER = logging.getLogger(__name__)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend(
    {
        vol.Required(CONF_WEATHER_SENSOR, default=""): cv.string,
        vol.Required(CONF_WEATHER_HOUR_BETWEEN, default=3): vol.All(
            vol.Coerce(int), vol.Range(min=1, max=24)
        ),
        vol.Optional(CONF_LABEL_SENSOR, default=COMPONENT_NAME): cv.string,
    }
)

# Add this in case of using Environment Canada weather
CONDITION_PICTURES_LOCAL = {
    "snowy-rainy": [
        "/static/images/darksky/weather-hail.svg",
        "mdi:weather-snowy-rainy",
    ],
    "rainy": ["/static/images/darksky/weather-pouring.svg", "mdi:weather-pouring"],
    "clear-night": ["/static/images/darksky/weather-night.svg", "mdi:weather-night"],
}


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""

    # latitude = config.get(CONF_LATITUDE, hass.config.latitude)
    # longitude = config.get(CONF_LONGITUDE, hass.config.longitude)
    # language = config.get(CONF_LANGUAGE)
    # interval = config.get(CONF_SCAN_INTERVAL, SCAN_INTERVAL)
    weather_sensor_name = config.get(CONF_WEATHER_SENSOR)
    intervale_entre_prevision = config.get(CONF_WEATHER_HOUR_BETWEEN)
    # Check if dependant sensor is ready
    if hass.states.get(weather_sensor_name) is None:
        raise PlatformNotReady(
            f"{COMPONENT_NAME} weather_sensor_name: {weather_sensor_name} not ready"
        )

    component_sensor_label_name = config.get(CONF_LABEL_SENSOR)

    weather_hourly_data = WeatherHourlyData(
        hass,
        interval=SCAN_INTERVAL,
        weather_hourly=weather_sensor_name,
        intervale_entre_prevision=intervale_entre_prevision,
    )

    # Force update as nothing happen otherwise
    try:
        weather_hourly_data.update()
    except:
        raise ValueError(f"{COMPONENT_NAME} Invalid values for {weather_sensor_name}")
    if weather_hourly_data.get_data(0) is None:
        raise PlatformNotReady(
            f"{COMPONENT_NAME} weather_sensor_name: {weather_sensor_name} not ready"
        )

    list_sensor = list()
    for count in range(int(24 / intervale_entre_prevision)):
        list_sensor.append(
            WeatherHourlyFormatSensor2(
                hass, component_sensor_label_name, index=count, data=weather_hourly_data
            )
        )
        # Force update
        list_sensor[-1].update()

    add_devices(list_sensor)


class WeatherHourlyFormatSensor2(Entity):
    """Representation of a Sensor."""

    def __init__(self, hass, component_sensor_label_name, index, data):
        """Initialize the sensor."""
        super(WeatherHourlyFormatSensor2, self).__init__()
        self._state = None
        self._hass = hass
        self._LOGGER = logging.getLogger(__name__)
        # Friendly name is managed by the name
        self._name = "{}_{}".format(component_sensor_label_name, index)
        self._index = index
        self._icon = None
        self._data = data
        self.entity_id = "{}.{}".format("sensor", self._name)
        self._condition_picture = dict(CONDITION_PICTURES)
        self._condition_picture.update(CONDITION_PICTURES_LOCAL)

    def __str__(self):
        return "name: {}, index: {}".format(self._name, self._index)

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return DEGREE

    @property
    def entity_picture(self):
        """Return the entity picture to use in the frontend, if any."""
        if self._icon is not None:
            if self._icon in self._condition_picture:
                return self._condition_picture[self._icon][0]

            return "/static/images/darksky/weather-{}.svg".format(self._icon)

        return None

    @property
    def icon(self):
        """Icon to use in the frontend, if any."""
        if self._icon is not None:
            return "mdi:weather-{}".format(self._icon)

        return None

    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        # self._LOGGER.info("==Update: {}".format(self._index))

        self._data.update()

        forecast = self._data.get_data(self._index)

        if forecast is not None:
            # date_obj = datetime.datetime.strptime(
            # forecast['datetime'], '%Y-%m-%dT%H:%M:%S')
            date_obj_convert = datetime.datetime.fromisoformat(
                str(forecast["datetime"])
            )
            date_obj = dt.as_local(date_obj_convert)

            if "temperature" in forecast:
                self._state = int(forecast["temperature"])
            self._name = "{}".format(date_obj.hour)
            self._icon = forecast[ATTR_FORECAST_CONDITION]
        else:
            # self._LOGGER.warn("No state identified")
            self._state = None


class WeatherHourlyData:
    """Get the data from external sensor."""

    def __init__(self, hass, interval, weather_hourly, intervale_entre_prevision):
        """Initialize the data object."""
        self._hass = hass
        self.data = list()
        self._weather_hourly = weather_hourly
        self._intervale_entre_prevision = intervale_entre_prevision
        self._number_of_values = int(24 / self._intervale_entre_prevision)

        self._LOGGER = logging.getLogger(__name__)

        # Apply throttling to methods using configured interval
        self.update = Throttle(interval)(self._update)

    def _update(self):
        """Get the latest data from Weather."""
        counter = 0

        all_forecast = self._hass.states.get(self._weather_hourly).attributes.get(
            "forecast"
        )

        # Manage error case of no response of server for a long time
        if all_forecast is None:
            self.data = list()
        else:
            for forecast in all_forecast:
                try:
                    date_obj_convert = datetime.datetime.fromisoformat(
                        str(forecast["datetime"])
                    )
                    date_obj = dt.as_local(date_obj_convert)
                    # date_obj = datetime.datetime.strptime(
                    # forecast['datetime'], '%Y-%m-%dT%H:%M:%S')
                    if int(date_obj.hour) % self._intervale_entre_prevision == 0:
                        # self._LOGGER.info("==Info added: {}".format(date_obj))
                        if len(self.data) < self._number_of_values:
                            self.data.append(forecast)
                        else:
                            self.data[counter] = forecast
                        counter += 1
                except ValueError as e:
                    self._LOGGER.error("Exception on conversion: {}".format(e))

                # Break loop to avoid more data
                if counter == self._number_of_values:
                    break

    def get_data(self, index):
        if index < len(self.data):
            return self.data[index]
        return None
