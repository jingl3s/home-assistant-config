# TODO config et autre

* [x] Mettre en place affichage des horaires STM
* [x] Mise a jour du systeme
* [x] Reoganiser bin/
* [x] Mettre en place demarrage de firefox au demarrage du PC

* [ ] Mise en place nouveau PC écran tactile
  * Valider Affichage infos
  * Reduction consommation électrique
  * Luminosité écran en ligne de commande
  * Luminosité depuis Home assistant
  * Voir branchement Haut parleur en sortie

* [ ] Intégration open meteo revoir timezone
  * https://github.com/home-assistant/core/tree/dev/homeassistant/components/open_meteo
  * https://github.com/frenck/python-open-meteo/blob/main/src/open_meteo/open_meteo.py

* Mise à jour musique dans list input
  * custom_components/update_music/main_music.py

* Mettre en place un boolean pour état internet qui se remet active 2 minutes après un retour d'internet
* Mise a jour composants au retour d'internet via une automatisation qui va ceduler les enchainements de mises a jour
* Mise ajour des composants désactivées pour toutes les integrations
  * Mise à jour via automatisation en enchainement toutes les X minutes avec une verification si la mise a jour est antérieur au refresh pour eviter des refresh intempestif 