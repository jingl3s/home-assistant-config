#/usr/bin/bash

pid=$(ps -ef |grep python|grep hass-configurator|sed -e's/  */ /g'|cut -f 2 -d ' ')

echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Hass configurator detruit"
fi

~/homeassistant_local/homeassistant/bin/hass-configurator ~/homeassistant_local/homeassistant/hass-configurator-settings.conf &