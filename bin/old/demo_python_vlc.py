import os
import time

import vlc

print(vlc.libvlc_get_version())

instance = vlc.Instance([])

mp3_file = "/home/hugo/homeassistant_local/homeassistant/tts/5e8405ef788f31da612371d0392dbe8ff2ce7659_fr_-_tts.google_translate_fr_ca.mp3"
os.environ["VLC_VERBOSE"] = str("0")
_vlc = instance.media_player_new()
_vlc.set_media(instance.media_new(mp3_file))
out = _vlc.play()
print(out)
# sleep to let vlc play the sound
time.sleep(10)