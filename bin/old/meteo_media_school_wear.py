#!/home/hugo/homeassistant_local/venv310_malloc/bin/python
# coding=utf-8
# https://www.geeksforgeeks.org/screenshot-element-method-selenium-python/
# import webdriver
import os
import sys
import time
from pathlib import Path
import logging
import urllib.request
from PIL import Image, ImageDraw, ImageFont
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.firefox.service import Service

OUTPUT_FILE = "/mnt/tmpfs/meteo_media_what_to_wear.png"

logger: logging.Logger = None


def crop_image(image: str):
    """
    source: https://gist.github.com/odyniec/3470977
    """
    logger.debug("reduit image")

    # Get input and output file names
    infile = image

    image = Image.open(infile)

    # Get the bounding box
    bbox = image.getbbox()

    # new_box = (29,0,bbox[2], bbox[3]-75)
    new_box = (29, 0, bbox[2], bbox[3] - 25)

    # Crop the image to the contents of the bounding box
    image = image.crop(new_box)

    # Determine the width and height of the cropped image
    (width, height) = image.size

    # Create a new image object for the output image
    cropped_image = Image.new("RGBA", (width, height), (0, 0, 0, 0))

    # Paste the cropped image onto the new image
    border = 0
    cropped_image.paste(image, (border, border))

    # Save the output image
    cropped_image.save(infile)


def _create_error_image() -> None:
    # subprocess.run(
    #     f"convert -background lightblue -fill black  -pointsize 170 label:Erreur {OUTPUT_FILE}_tmp.png"
    # )
    logger.debug("Creation image erreur")
    img = Image.new("RGB", (600, 200), color=(73, 109, 137))

    d = ImageDraw.Draw(img)
    fnt = ImageFont.truetype(
        "/usr/share/fonts/truetype/freefont/FreeMono.ttf", size=120
    )
    d.text((10, 10), "Erreur", fill=(255, 255, 0), font=fnt)
    if os.path.exists(OUTPUT_FILE):
        os.remove(OUTPUT_FILE)
    img.save(OUTPUT_FILE)


def is_url_available(url):
    try:
        response = urllib.request.urlopen(url)
        return response.getcode() == 200
    except urllib.error.URLError:
        return False


def update_image(url: str):
    """
    # Utilisation de selenium car pas possible sans interpréteur js
    """

    logger.debug("Demarrage navigateur")

    if not is_url_available(url):
        return

    # create webdriver object
    try:
        logger.info("Firefox démarrage")
        options = FirefoxOptions()
        options.add_argument("--headless")
        ser = Service("/home/hugo/bin/geckodriver")
        driver = webdriver.Firefox(options=options, service=ser)
        logger.info("Firefox disponible")
    except Exception as err:
        logger.debug(err)
        logger.info("Chromium démarrage")
        try:
            options = webdriver.ChromeOptions()
            options.add_argument("--headless")
            driver = webdriver.Chrome(options=options)
            logger.info("Chromium disponible")
        except Exception:
            logger.exception("Error start chromium")

    try:
        logger.info("Recuperation information page")

        # get geeksforgeeks.org
        driver.get(url)

        try:
            btn_element = WebDriverWait(driver, 240).until(
                EC.presence_of_element_located((By.CLASS_NAME, "beta_popup_close_btn"))
            )
            btn_element.click()
        except Exception as err:
            logger.error(err)

        # get element
        # element = driver.find_element(By.CLASS_NAME, "bx-wrapper")
        element = WebDriverWait(driver, 120).until(
            EC.presence_of_element_located((By.CLASS_NAME, "bx-wrapper"))
        )
        # element = driver.find_element_by_id("school-to-wear")
        # element =driver.find_element(By.ID, 'school-to-wear')

        # click screenshot
        element.screenshot(f"{OUTPUT_FILE}_tmp.png")

        if os.path.exists(OUTPUT_FILE):
            os.remove(OUTPUT_FILE)
        os.rename(f"{OUTPUT_FILE}_tmp.png", OUTPUT_FILE)
        crop_image(OUTPUT_FILE)
    except Exception:
        logger.exception("Erreur recuperation")
        p = Path(OUTPUT_FILE)
        diff_t = time.time() - p.stat().st_ctime
        if diff_t > 24 * 60 * 60:
            _create_error_image()
    except NoSuchElementException:
        logger.exception("Erreur Element absent")
        _create_error_image()
        driver.quit()
        raise

    finally:
        driver.quit()
    logger.info("termine")


if __name__ == "__main__":
    log_file = Path("/mnt/tmpfs/") / f"{Path(__file__).stem}.log"
    logging.basicConfig(
        filename=log_file,
        format="%(asctime)s-%(levelname)7s-%(module)s-%(funcName)s-%(message)s",
        level=logging.INFO,
        filemode="w",
    )
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    try:
        update_image(
            "https://www.meteomedia.com/ca/previsions/scolaires/quebec/ecole-primaire-iona"
        )
    except Exception as e:
        print(e)
        sys.exit(1)
    sys.exit(0)
