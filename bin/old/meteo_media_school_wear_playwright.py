# coding=utf-8
import os
import time
import sys
import logging
from pathlib import Path
from PIL import Image, ImageDraw, ImageFont
from playwright.sync_api import Playwright, sync_playwright

OUTPUT_FILE = "/mnt/tmpfs/meteo_media_what_to_wear.png"

logger: logging.Logger = None


def crop_image(image: str):
    """
    source: https://gist.github.com/odyniec/3470977
    """
    logger.debug("reduit image")

    # Get input and output file names
    infile = image

    image = Image.open(infile)

    # Get the bounding box
    bbox = image.getbbox()

    # new_box = (29,0,bbox[2], bbox[3]-75)
    new_box = (29, 0, bbox[2], bbox[3] - 25)

    # Crop the image to the contents of the bounding box
    image = image.crop(new_box)

    # Determine the width and height of the cropped image
    (width, height) = image.size

    # Create a new image object for the output image
    cropped_image = Image.new("RGBA", (width, height), (0, 0, 0, 0))

    # Paste the cropped image onto the new image
    border = 0
    cropped_image.paste(image, (border, border))

    # Save the output image
    cropped_image.save(infile)


def _create_error_image() -> None:
    # subprocess.run(
    #     f"convert -background lightblue -fill black  -pointsize 170 label:Erreur {OUTPUT_FILE}_tmp.png"
    # )
    logger.debug("Creation image erreur")
    img = Image.new("RGB", (600, 200), color=(73, 109, 137))

    d = ImageDraw.Draw(img)
    fnt = ImageFont.truetype(
        "/usr/share/fonts/truetype/freefont/FreeMono.ttf", size=120
    )
    d.text((10, 10), "Erreur", fill=(255, 255, 0), font=fnt)
    if os.path.exists(OUTPUT_FILE):
        os.remove(OUTPUT_FILE)
    img.save(OUTPUT_FILE)


def run(playwright: Playwright, url: str) -> None:
    logger.debug("Demarrage navigateur")

    head_less = True
    # head_less = False
    # browser = playwright.chromium.launch(headless=head_less)
    # Firefox ne fonctionne pas
    browser = playwright.firefox.launch(headless=head_less)
    context = browser.new_context()
    context.set_default_timeout(200000)

    logger.info("Recuperation information page")

    try:
        # Open new page
        page = context.new_page()

        page.goto(url)

        page.wait_for_load_state()
    except TimeoutError:
        logger.exception("Erreur chargement page")
        _create_error_image()
        raise
    except Exception:
        logger.exception("Erreur recuperation")
        p = Path(OUTPUT_FILE)
        diff_t = time.time() - p.stat().st_ctime
        if diff_t > 24 * 60 * 60:
            _create_error_image()

    try:
        element = page.locator(".bx-wrapper").first
        element.screenshot(path=f"{OUTPUT_FILE}_tmp.png")
    except TimeoutError:
        logger.exception("Erreur Element absent")
        _create_error_image()
        raise
    except Exception:
        logger.exception("Erreur recuperation")
        p = Path(OUTPUT_FILE)
        diff_t = time.time() - p.stat().st_ctime
        if diff_t > 24 * 60 * 60:
            _create_error_image()

    context.close()
    browser.close()


def update_image_playwright(url: str):
    """ """

    with sync_playwright() as playwright:
        run(playwright, url)

    if os.path.exists(OUTPUT_FILE):
        os.remove(OUTPUT_FILE)
    os.rename(f"{OUTPUT_FILE}_tmp.png", OUTPUT_FILE)


if __name__ == "__main__":
    log_file = Path("/mnt/tmpfs/") / f"{Path(__file__).stem}.log"
    logging.basicConfig(
        filename=log_file,
        format="%(asctime)s-%(levelname)7s-%(module)s-%(funcName)s-%(message)s",
        level=logging.INFO,
        filemode="w",
    )
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    try:
        update_image_playwright(
            "https://www.meteomedia.com/ca/previsions/scolaires/quebec/ecole-primaire-iona"
        )
        crop_image(OUTPUT_FILE)
    except Exception:
        logger.exception("Erreur principale")
        sys.exit(1)
    sys.exit(0)
