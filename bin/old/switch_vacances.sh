#!/bin/bash

echo $1


cd ~/homeassistant_local/homeassistant

if [[ $1 == "state" ]] ; then
  if [[ -L configuration.yaml ]] ; then
    exit 0
  else
    exit 1
  fi
fi


if [[ $1 == "vacances" ]] ; then

  echo "Mode vacances"
  
  if [[ ! -L configuration.yaml ]] ; then
    cp configuration.yaml configuration_$(date "+%Y%m%d_%H%M").yaml 
    mv configuration.yaml configuration_normal.yaml 
    ln -s configuration-vacances.yaml configuration.yaml
  fi
  
  if [[ ! -L ui-lovelace.yaml ]] ; then
    cp ui-lovelace.yaml ui-lovelace_$(date "+%Y%m%d_%H%M").yaml 
    mv ui-lovelace.yaml ui-lovelace_normal.yaml 
    ln -s ui-lovelace-vacances.yaml ui-lovelace.yaml
  fi
  
  if [[ ! -L home-assistant_v2 ]] ; then
    mv home-assistant_v2 home-assistant_v2_normal 
    if [[ -e home-assistant_v2_vacances ]] ; then
      ln -s home-assistant_v2_vacances home-assistant_v2
    fi
  fi
  
  
else
  echo "Mode normal"
  if [[ -L configuration.yaml ]] ; then
    rm configuration.yaml
    mv configuration_normal.yaml configuration.yaml 
  fi
  
  if [[ -L ui-lovelace.yaml ]] ; then
    rm ui-lovelace.yaml
    mv  ui-lovelace_normal.yaml  ui-lovelace.yaml 
  fi
  
  if [[ -L home-assistant_v2 ]] ; then
    if [[ ! -e home-assistant_v2_vacances ]] ; then
      mv home-assistant_v2 home-assistant_v2_vacances
    else
      rm home-assistant_v2  
    fi
    mv home-assistant_v2_normal home-assistant_v2 
  fi

fi
exit 0
