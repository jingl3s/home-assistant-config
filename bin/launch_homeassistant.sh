#!/usr/bin/bash

pid=$(ps -ef |grep python|grep '\-m homeassistant' |sed -e's/  */ /g'|cut -f 2 -d ' ')

echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Home assistant"
fi


CUR_IP=$(ip route get 1| awk '{print $NF;exit}')
CUR_DIR=$(dirname $0)
cd $CUR_DIR

# if [[ "$CUR_IP" == "192.168.254.196" ]] ;then
#   bash switch_vacances.sh normal
#   # sh ~/projects/AfficheInfos/src/scripts/termux_affich_info_web.sh &
# # else
# #   bash switch_vacances.sh vacances
# #   CUR_IP="localhost"
# fi

echo $USER
groups


export MY_HOMEASSISTANT_PATH=/home/hugo/homeassistant_local/homeassistant
export MY_HOMEASSISTANT_PYTHON=/home/hugo/homeassistant_local/venv/bin/python

#sudo systemctl restart home-assistant@hugo
$MY_HOMEASSISTANT_PYTHON -m homeassistant -c $MY_HOMEASSISTANT_PATH


# bash $MY_HOMEASSISTANT_PATH/bin/restart_browser.sh

#sh $MY_HOMEASSISTANT_PATH/git_scripts/lufa-status/src/scripts/launch_script.sh &
