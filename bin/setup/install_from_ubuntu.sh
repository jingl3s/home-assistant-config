#/usr/bin/bash

HOMEASSISTANT_INSTALL=~/homeassistant_local/homeassistant

# python=~/homeassistant/bin/python
python=~/homeassistant_local/venv/bin/python

# Utilise par restart_browser.sh
sudo apt install xautomation

ls -l $python

$python --version

$python -m pip install --upgrade pip
$python -m pip install bs4 wheel hass-configurator
$python -m pip install git+https://github.com/titilambert/pyhydroquebec

# $python -m pip install --upgrade homeassistant home-assistant-frontend 
sh $HOMEASSISTANT_INSTALL/bin/update.sh

mkdir -p $HOMEASSISTANT_INSTALL/git_scripts/
#git clone https://gitlab.com/zorbac/lufa-status.git $HOMEASSISTANT_INSTALL/git_scripts/lufa-status
cd $HOMEASSISTANT_INSTALL/git_scripts/lufa-status
#git pull origin

$python -m pip install -r pip3-requires.txt
cd -


#git clone https://github.com/jingl3s/python-router-dlink-dir822.git $HOMEASSISTANT_INSTALL/git_scripts/python-router-dlink-dir822
cd $HOMEASSISTANT_INSTALL/git_scripts/python-router-dlink-dir822
#git pull origin
$python -m venv venv
./venv/bin/python3 -m pip install -r requirements.txt
cd -


# Install pour meteo_media_school_wear
# Pillow is part of homeassistant dependencies
# $python -m pip install pillow
type geckodriver
if [ $? -ne 0 ]; then
    $python -m pip install bs4 selenium
    sudo apt install chromedriver
    GECKO_VERSION=0.32.2
    GECKODRV=geckodriver-v${GECKO_VERSION}-linux64.tar.gz
    # curl https://github.com/mozilla/geckodriver/releases/download/v0.27.0/$GECKODRV -o /mnt/tmpfs/geckodriver-linux64.tar.gz
    cd /mnt/tmpfs/
    wget https://github.com/mozilla/geckodriver/releases/download/v${GECKO_VERSION}/${GECKODRV}
    tar xzf $GECKODRV
    mv geckodriver /$HOME/bin
    rm $GECKODRV
    cd -
fi


mkdir -p ~/homeassistant_local/media/musique
mkdir -p ~/homeassistant_local/media/dl
mkdir -p ~/homeassistant_local/media/podcasts

cd $HOMEASSISTANT_INSTALL
wget -O - https://get.hacs.xyz | bash -
cd -

#fstab
#tmpfs /mnt/tmpfs tmpfs defaults,noatime,nosuid,size=10m 0 0
