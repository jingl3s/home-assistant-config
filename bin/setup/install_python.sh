#/usr/bin/bash


https://community.home-assistant.io/t/python-3-12-backport-for-debian-12-bookworm/709459

https://docs.posit.co/resources/install-python-source.html

# cd ~/downloads
# #wget https://www.python.org/ftp/python/3.12.3/Python-3.12.3.tgz

sudo apt-get install \
    curl \
    gcc \
    libbz2-dev \
    libev-dev \
    libffi-dev \
    libgdbm-dev \
    liblzma-dev \
    libncurses-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    make \
    tk-dev \
    wget \
    zlib1g-dev

export PYTHON_VERSION=3.12.7
export PYTHON_MAJOR=3

curl -O https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar -xvzf Python-${PYTHON_VERSION}.tgz
cd Python-${PYTHON_VERSION}

./configure \
    --prefix=/opt/python/${PYTHON_VERSION} \
    --enable-shared \
    --enable-optimizations \
    --enable-ipv6 \
    LDFLAGS=-Wl,-rpath=/opt/python/${PYTHON_VERSION}/lib,--disable-new-dtags



# #tar xzf Python-3.12.3.tgz
# cd Python-3.12.3
# make clean
# sudo ./configure --enable-optimizations --with-lto  --disable-test-modules --with-computed-gotos --enable-ipv6 \
    LDFLAGS=-Wl,-rpath=/opt/python/${PYTHON_VERSION}/lib,--disable-new-dtags --prefix=/opt/python/${PYTHON_VERSION}
# make -j 1
# # sudo make install
# sudo make altinstall


#mv ~/homeassistant_local/venv ~/homeassistant_local/venv_prior

#/opt/python-3.12.3/bin/python3.12 -m venv ~/homeassistant_local/venv

# python=~/homeassistant/bin/python
#python=~/homeassistant_local/venv/bin/python



# Installation de lxml 5.1.1 pour retro compatible avec CPU qui est d'avant 2010
# https://stackoverflow.com/questions/78253701/python-illegal-instruction-core-dumped-when-importing-certain-libraries-bea

#$python -m pip install --upgrade pip
#$python -m pip install bs4 wheel hass-configurator numpy lxml==5.1.1
#sh ~/homeassistant_local/homeassistant/bin/update.sh
