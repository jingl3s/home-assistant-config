#/usr/bin/bash

HOMEASSISTANT_INSTALL=~/homeassistant_local/homeassistant

# systeme base installation
#curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
#sudo apt install bluez libffi-dev libssl-dev libjpeg-dev zlib1g-dev autoconf build-essential libopenjp2-7 libtiff6 libturbojpeg0-dev tzdata ffmpeg liblapack3 liblapack-dev libatlas-base-dev
# pip install wheel
# export MAKEFLAGS="-j1"
#sudo apt install xserver-xorg-input-synaptics
#sudo apt install libopenblas-dev
#pip install numpy==1.26.0 --verbose
# sudo nano /etc/default/grub
#  set GRUB_TIMEOUT=0
# sudo update-grub
# sudo apt install vlc
# utilise par command line scripts
#sudo apt install bc smem
#pip install open-meteo pychromecast env_canada
#sudo nano /etc/fstab
# Ajour
# tmpfs /mnt/tmpfs tmpfs defaults,noatime,nosuid,size=10m 0 0
# to avoid some disk sleep in the system
# source https://askubuntu.com/questions/1273679/kernel-is-spewing-ata-link-up-messages-every-second
# sudo nano /etc/crontab
# 0 0 * * * root echo max_performance | tee /sys/class/scsi_host/host0/link_power_management_policy
# @reboot root echo max_performance | tee /sys/class/scsi_host/host0/link_power_management_policy
# S'assurer d'avoir cron dans les service runit voir comment installer le service home assistant dans runit
# Apres reboot
# cat /sys/class/scsi_host/host*/link_power_management_policy
# Pour configurer le startup apres login
# sudo apt install jwmkit
# jwmkit_startup
# cp jwm/startup /home/hugo/.config/jwm
# Si jwm se reset, il faut supprimer le dossier .jwm present dans .config
# jwmkit peut ne pas fonctionner car fichier n'est pas bien configuré
# jwmkit_repair permet de voir le fichier erroné, le souci se passe avec le charactère & qu'il faut remplacer par &amp;
# sudo xteapt install xautomation
# xset s 600 dpms 0 0 600
# xset -q
# apt install brightnessctl
# sudo usermod -a -G audio hugo




# python=~/homeassistant/bin/python
python=~/homeassistant_local/venv/bin/python

# Utilise par restart_browser.sh
sudo apt install xautomation

ls -l $python

$python --version

$python -m pip install --upgrade pip
$python -m pip install bs4 wheel hass-configurator lxml==5.1.1 selenium
# Remplacement https://gitlab.com/hydroqc/hydroqc/-/blob/main/examples/hydro.py?ref_type=heads
# $python -m pip install git+https://github.com/titilambert/pyhydroquebec

# $python -m pip install --upgrade homeassistant home-assistant-frontend 
sh $HOMEASSISTANT_INSTALL/bin/update.sh

#HACS
wget -O - https://get.hacs.xyz | bash -

mkdir -p $HOMEASSISTANT_INSTALL/git_scripts/
git clone https://gitlab.com/zorbac/lufa-status.git $HOMEASSISTANT_INSTALL/git_scripts/lufa-status
cd $HOMEASSISTANT_INSTALL/git_scripts/lufa-status
git pull origin

$python -m pip install -r pip3-requires.txt
cd -


git clone https://github.com/jingl3s/python-router-dlink-dir822.git $HOMEASSISTANT_INSTALL/git_scripts/python-router-dlink-dir822
cd $HOMEASSISTANT_INSTALL/git_scripts/python-router-dlink-dir822
git pull origin
$python -m venv venv
./venv/bin/python3 -m pip install -r requirements.txt



cd -


# # Install pour meteo_media_school_wear
# # Pillow is part of homeassistant dependencies
# # $python -m pip install pillow
# type geckodriver
# if [ $? -ne 0 ]; then
#     $python -m pip install bs4 selenium
#     #sudo apt install chromedriver
#     sudo apt install ungoogled-chromium-driver
#     GECKO_VERSION=0.35.0
#     #GECKODRV=geckodriver-v${GECKO_VERSION}-linux64.tar.gz
#     GECKODRV=geckodriver-v${GECKO_VERSION}-linux32.tar.gz
#     cd /mnt/tmpfs/
#     wget https://github.com/mozilla/geckodriver/releases/download/v${GECKO_VERSION}/${GECKODRV}
#     tar xzf $GECKODRV
#     mkdir $HOME/bin >/dev/null 2>&1
#     mv geckodriver $HOME/bin/
#     rm $GECKODRV
#     cd -
# fi
