#/usr/bin/bash

python=~/homeassistant_local/venv/bin/python

ls -l $python

# $python --version
# $python -m pip install --upgrade homeassistant home-assistant-frontend 


# https://zapier.com/engineering/celery-python-jemalloc/


# LD_PRELOAD=/mnt/1T/01_Documents_Secondaires/Programmation/GitLab_Projets/home-assistant-config/jemalloc-5.3.0/lib/libjemalloc.so ./venv310_malloc/bin/python -m compileall venv310_malloc/lib/python3.10/site-packages/
# LD_PRELOAD=/mnt/1T/01_Documents_Secondaires/Programmation/GitLab_Projets/home-assistant-config/jemalloc-5.3.0/lib/libjemalloc.so ./venv310_malloc/bin/python -m compileall custom_components
# LD_PRELOAD=/mnt/1T/01_Documents_Secondaires/Programmation/GitLab_Projets/home-assistant-config/jemalloc-5.3.0/lib/libjemalloc.so ./venv310_malloc/bin/hass -c .

cd ~/homeassistant_local/
python=/opt/python310/bin/python3
$python -m venv venv310_malloc
LD_PRELOAD="/home/hugo/homeassistant_local/jemalloc-5.3.0/lib/libjemalloc.so" \
MALLOC_CONF="background_thread:true,metadata_thp:auto,dirty_decay_ms:20000,muzzy_decay_ms:20000" \
./venv310_malloc/bin/pip3 install --upgrade \
           --use-deprecated=legacy-resolver \
           homeassistant home-assistant-frontend selenium


LD_PRELOAD="/home/hugo/homeassistant_local/jemalloc-5.3.0/lib/libjemalloc.so" \
./venv310_malloc/bin/python -m compileall ./venv310_malloc/lib/python3.10/site-packages/

# LD_PRELOAD="/home/hugo/homeassistant_local/jemalloc-5.3.0/lib/libjemalloc.so" \
# ./venv310_malloc/bin/python -m compileall homeassistant/custom_components


# /etc/systemd/system/home-assistant@hugo.service                                                       

# [Unit]
# Description=Home Assistant
# After=network-online.target

# [Service]
# Type=simple
# User=%i
# WorkingDirectory=/home/%i/homeassistant_local/homeassistant
# #ExecStart=/home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/venv/bin/hass -c /home/hugo/homeassistant_local/homeassistant
# #ExecStart=/home/%i/homeassistant_local/venv310_malloc/bin/python /home/%i/homeassistant_local/venv310_malloc/bin/hass -c /home/%i/homeassistant_local/homeassist>
# ExecStart=/home/%i/homeassistant_local/homeassistant/bin/start_hass.sh
# RestartForceExitStatus=100

# [Install]
# WantedBy=multi-user.target


# systemctl daemon-reload
# sudo systemctl status home-assistant@hugo.service