#/usr/bin/bash

python=~/homeassistant_local/venv/bin/python

ls -l $python

$python --version
$python -m pip install --upgrade homeassistant home-assistant-frontend 

cd ~/homeassistant_local/homeassistant
wget -O - https://get.hacs.xyz | bash -
cd -
