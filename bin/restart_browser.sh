#/usr/bin/bash

export DISPLAY=:0.0

killall chromium > /dev/null 2>&1
# killall firefox-bin > /dev/null
killall firefox-esr > /dev/null  2>&1
killall firefox > /dev/null  2>&1
sleep 10

#Site pour informations sur la connexion courante
#http://ifconfig.co/
# CUR_IP=$(ip route get 1| awk '{print $NF;exit}')
# CUR_IP="192.168.254.196"
# if [[ "$CUR_IP" != "192.168.254.196" ]] ;then
#   CUR_IP="localhost"
# fi
CUR_IP="localhost"

#HOMEASSISTANT_PAGE="ui-bus-yaml/0?kiosk"
HOMEASSISTANT_PAGE="ui-infos-yaml/0"

# firefox-esr -url http://$CUR_IP:8123/$HOMEASSISTANT_PAGE &
firefox -url http://$CUR_IP:8123/$HOMEASSISTANT_PAGE --kiosk --height=600 >/dev/null 2>/dev/null &
# /opt/firefox/firefox-bin -url http://$CUR_IP:8123/$HOMEASSISTANT_PAGE --kiosk >/dev/null 2>/dev/null &

# midori -e Fullscreen -e Navigationbar http://$CUR_IP:8123/$HOMEASSISTANT_PAGE >/dev/null 2>/dev/null &

# https://peter.sh/experiments/chromium-command-line-switches/
# chromium http://$CUR_IP:8123/$HOMEASSISTANT_PAGE --start-maximized --start-fullscreen &
# URL pour optimiser Chrome
# https://github.com/jeffeb3/olkb-terminal
# https://stackoverflow.com/questions/66607707/reduce-chrome-chromium-memory-usage
#chromium --disable-gpu --kiosk --noerrdialogs --enable-features=OverlayScrollbar --disable-restore-session-state  http://$CUR_IP:8123/$HOMEASSISTANT_PAGE &
# chromium --kiosk --noerrdialogs --enable-features=OverlayScrollbar --disable-restore-session-state --lang=FR http://$CUR_IP:8123/$HOMEASSISTANT_PAGE &
#chromium --kiosk --noerrdialogs --enable-features=OverlayScrollbar --disable-restore-session-state http://$CUR_IP:8123/$HOMEASSISTANT_PAGE &

until $(curl --output /dev/null --silent --head http://$CUR_IP:8123); do
    printf '.'
    sleep 5
done

# Attendre que Home assistant soit disponible pour rafraichir la fenetre
# Pour historique car utilisation de kiosk du navigateur ne demande plus ces manipulations
# sleep 20; xdotool search --sync --onlyvisible --class "chromium" windowactivate keydown F5 &

# sleep 60; xdotool search --sync --onlyvisible --class "Firefox" windowactivate key F11 &
# sleep 60; xdotool search --sync --onlyvisible --class "Firefox" windowactivate key F5 &

sleep 40

# xte "keydown Alt_L" "key Tab" "keyup Alt_L"
# xte "keydown Alt_L" "key Tab" "keyup Alt_L"

# Definition extinction ecran apres 10 minutes
xset s 600 dpms 0 0 600
# Activation ecran de veille force car sinon il reste eclairé
xset s activate
