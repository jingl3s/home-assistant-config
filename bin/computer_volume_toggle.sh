#/usr/bin/sh
# REcuperation de l'etat du mixer, Master seulement non fonctionnel en non utilisateur par defaut
# amixer sur session root renvoi plus de sorties dont Speaker
# amixer mute coupe toutes les sorties et le toggle doit se faire sur tous
# Solution : https://askubuntu.com/questions/65764/how-do-i-toggle-sound-with-amixer#286437
# Utiliser la commande suivante en avant / apres dans un compte root ou utilisateur
# amixer scontents > ~/before

/usr/bin/amixer get Master |grep \\[off\\] > /dev/null
MASTER=$?
/usr/bin/amixer get Speaker |grep \\[off\\] > /dev/null
SPEAKER=$?
VAL_COMPUT=$(bc <<< $MASTER+$SPEAKER)
#echo "MASTER: $MASTER, SPEAKER: $SPEAKER, VAL_COMPUT: $VAL_COMPUT"

if [ "$1" == "get" ] ; then
  exit $VAL_COMPUT
fi

if [ $VAL_COMPUT -eq 0 ] ; then
  /usr/bin/amixer set Master toggle > /dev/null
  /usr/bin/amixer set Speaker toggle > /dev/null
else
  /usr/bin/amixer set Master mute > /dev/null
fi
