#/usr/bin/bash

#Site pour informations sur la connexion courante
#http://ifconfig.co/

pid=$(ps -ef |grep python|grep /homeassistant/__main|sed -e's/  */ /g'|cut -f 2 -d ' ')

echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Hass detruit"
fi

pid=$(ps -ef |grep python|grep -e '-m homeassistant'|sed -e's/  */ /g'|cut -f 2 -d ' ')

echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Hass detruit"
fi

pid=$(ps -ef |grep python|grep -e 'hass -c'|sed -e's/  */ /g'|cut -f 2 -d ' ')

echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Hass detruit"
fi

CUR_IP=$(ip route get 1| awk '{print $NF;exit}')
CUR_DIR=$(dirname $0)
cd $CUR_DIR

echo $CUR_IP > /mnt/tmpfs/start_ip.txt

if [[ "$CUR_IP" == "192.168.254.196" ]] ;then
  bash switch_vacances.sh normal
  # sh ~/projects/AfficheInfos/src/scripts/termux_affich_info_web.sh &
  
# else
#   bash switch_vacances.sh vacances
#   CUR_IP="localhost"
fi

#. ~/homeassistant/bin/activate ; hass --open-ui &
# . ~/homeassistant/bin/activate ; hass &
# ~/homeassistant/bin/python -m homeassistant &

# Use of full command as it is the same when using a restart from Home assistant from end
# /home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/venv/lib/python3.10/site-packages/homeassistant/__main__.py -c /home/hugo/homeassistant_local/homeassistant &
# /home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/venv/bin/hass -c /home/hugo/homeassistant_local/homeassistant &
# LD_PRELOAD=/usr/local/lib/libjemalloc.so /home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/venv/bin/hass -c /home/hugo/homeassistant_local/homeassistant &
# https://zapier.com/engineering/celery-python-jemalloc/
# LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2 /home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/venv/bin/hass -c /home/hugo/homeassistant_local/homeassistant >/dev/null 2>/dev/null &
#sudo systemctl restart home-assistant@hugo
/home/hugo/homeassistant_local/venv/bin/python -m homeassistant -c /home/hugo/homeassistant_local/homeassistant &


bash /home/hugo/homeassistant_local/homeassistant/bin/restart_browser.sh

#sh /home/hugo/homeassistant_local/homeassistant/git_scripts/lufa-status/src/scripts/launch_script.sh &



# ~/homeassistant_local/homeassistant/bin/python /home/hugo/homeassistant_local/venv/lib/python3.8/site-packages/homeassistant/__main__.py &
# bash ~/homeassistant_local/homeassistant/bin/restart_browser.sh
# sh ~/homeassistant_local/homeassistant/git_scripts/lufa-status/src/scripts/launch_script.sh &
