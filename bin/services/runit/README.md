## Installation

```shell
sudo mkdir /var/log/runit/homeassistant
sudo cp -r homeassistant/ /etc/sv/
# To use espeak-ng as service otherwize replace espeak-ng by espeak
sudo chmod 777 /root
sudo chmod 777 /root/.config
sudo chmod 777 /root/.config/pulse/
```

Utilisation de `Gestionnaire de service Runit` ou aussi en shell `/usr/local/bin/runit-service-manager.sh`. Il permet d'installer et lancer des configuration runit.

Il est disponible dans Dawn Small Linux via le gestionnaire aussi accessible avec `dslcc.sh`

## Documentations

- https://wiki.gentoo.org/wiki/Runit
- https://kchard.github.io/runit-quickstart/
- https://docs.voidlinux.org/config/services/user-services.html
  - Celui ci a permit de fixer un souci avec le module vlc depuis tts dans homeassistant
- https://wiki.archlinux.org/title/Runit#User_Services

## Interfaces

- https://codeberg.org/daltomi/xrunit
- runit-service-manager.sh disponible dans DSL

## Workarounds

runit est lancé en mode utilisateur avec tous les groupes de l'utilisateur.

### Investigations accès a la carte son et la gestion de la lumière depuis service par défaut

Lors de la premiere mise en place du service avec runit, il n'était pas possible de controler la luminosité de l'écran et sortir du son avec vlc via tts.

Ci-dessous les tentatives avec chpst et l'utilisation de groupes. Cela fonctionnait très bien en ligne de commande mais via le service non.
La solution a été de lancer le service en définissant l'utilisateur et les groupes qu'il pouvait utiliser.

Fait dans un script

```shell
# /usr/bin/amixer -q sset "Master" 50%
# /usr/bin/aplay -l >>/mnt/tmpfs/test.log 2>&1
env >>/mnt/tmpfs/test.log 2>&1
```

```shell
sudo chpst -u hugo:audio -U hugo:hugo aplay -l

# Final solution
sudo chpst -u hugo:hugo:audio -U hugo:hugo:audio:video aplay -l
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video /usr/bin/brightnessctl -d intel_backlight s 50
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  /usr/bin/espeak-ng -vfr "test" -p 30 -g 2
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  cvlc 06e32972fea0f083c183b378cbc666876dba89e7_fr-ca_-_google_translate.mp3
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  /home/hugo/homeassistant_local/venv/bin/python /home/hugo/homeassistant_local/homeassistant/bin/old/demo_python_vlc.py
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  alsamixer
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  /usr/bin/amixer get "Master"
sudo chpst -u hugo:hugo:audio:video -U hugo:hugo:audio:video  /usr/bin/amixer -q sset "Master" 90%
```

Workaround for audio non utilisé

```shell
sudo chmod o+rw /dev/snd/*
```

Workaround solution idée
https://skarnet.org/lists/supervision/2248.html
