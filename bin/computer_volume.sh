#/usr/bin/sh
/usr/bin/amixer get Master  | grep dB|cut -d'[' -f 2|cut -d'%' -f 1

# Other ways not working as using a service without access to pulse
#/usr/bin/amixer -D pulse get Master | awk -F 'Left:|[][]' 'BEGIN {RS=""}{ print $3 }'| sed 's/[%|,]//g'
#/usr/bin/pacmd list-sinks|grep -A 15 '* index'| awk '/volume: front/{ print $5 }' | sed 's/[%|,]//g'
