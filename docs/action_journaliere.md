# Action tous les jour

## Objectif

Avoir un bouton cliquable le matin qui va appeler un script pour réaliser une action a tous les jours.
Le bouton se réinitialise tous les jours.
La disponibilité du bouton est seulement les jours de semaine jusqu'a 9h du matin.


## Configuration Home assistant

### Script appelé

* Emplacement: bin/daily_email.py
  Le script n'est pas disponible dans le repos car informations personnelles
* Entité Home Assistant : entities/switches/daily_email.yaml
  Command line qui utilise python et le script

```yaml
platform: command_line
switches:
  daily_email:
    command_on: "python3 /home/hugo/homeassistant_local/homeassistant/bin/daily_email.py"
```


### Home assistant automation

* Input boolean : entities/input_booleans/daily_email_sent.yaml
  Sauvegarde l'état si envoyé

```yaml
daily_email_sent:
  name: Daily email sent
  initial: on
```

* Automatisation pour change l'indication du switch quand effectué

```yaml
  alias: Daily_email_disable
  description: Desactive le bouton d'email lorsqu'il a été activé
  trigger:
  - entity_id: switch.daily_email
    for: '60'
    from: 'off'
    platform: state
    to: 'on'
  condition: []
  action:
  - data: {}
    entity_id: input_boolean.daily_email_sent
    service: input_boolean.turn_on
  mode: single
```

* Reset l'état du input boolean pour indiquer disponible tous les jours à 5h

```yaml
  alias: Daily_reset_email_status
  description: ''
  trigger:
  - at: '5:00'
    platform: time
  condition: []
  action:
  - data: {}
    entity_id: input_boolean.daily_email_sent
    service: input_boolean.turn_off
  - data: {}
    entity_id: switch.daily_email
    service: switch.turn_off
  mode: single
```

* Désactive le bouton automatiquement à 9h

```yaml
  alias: Daily_email_reset_status_disable
  description: Descativation du bouton email en cas de jour non travaile
  trigger:
  - at: '9:00'
    platform: time
  condition: []
  action:
  - data: {}
    entity_id: input_boolean.daily_email_sent
    service: input_boolean.turn_on
  mode: single
```

* Automatisation musique_matin
  * Tous les jours 7:10
  * Jour de semaine
  * Definie le niveau sonore du PC
  * Regarde l'état de spotify et appel le script selon dispo
    * script.musique_spotify
    * script.musique_vlc

### Affichage dans Lovelace (fenêtres Home Assistant)

* Cartes Lovelace utilisé depuis les cartes en code uniquement
  * lovelace/components/daily_email.yaml
  * Horizontal stack 
  * Condition
    * Jour semaine
    * Non envoyé
    * hold_action et tap_action avec confirmation pour ne pas envoyé alors que non attendu
    * hold_action utilisé pour indiquer que non applicable ce jour en changeant le input boolean


```yaml
type: conditional
card: 
    entity: switch.daily_email
    hold_action:
      action: call-service
      service: input_boolean.turn_on
      service_data:
        entity_id: input_boolean.daily_email_sent
      confirmation:
        text: Es tu sure de ne pas travailler aujourd'hui ?
    icon: 'mdi:email'
    show_icon: true
    show_name: true
    tap_action:
      action: toggle
      confirmation:
        text: Veux tu envoyer le courriel ?
    type: button
    icon_height: 150px
    name: Envoi email journalier
conditions:
  - entity: binary_sensor.jours_travail
    state: 'on'
  - entity: input_boolean.daily_email_sent
    state: 'off'
```

### Mode maintenance en cas d'oublie ou d'anomalie pour faire un reset de l'état

```yaml
  - type: entities
    title: Travail
    show_header_toggle: false
    entities:
      - entity: input_boolean.daily_email_sent
      - entity: binary_sensor.jours_travail
      - entity: switch.daily_email
```