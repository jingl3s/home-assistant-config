# Meteo media grab and display what to wear today at school

## Objectif
* Afficher les vetements à porter pour aller à l'école
  * https://www.meteomedia.com/ca/previsions/scolaires/quebec/ecole-iona
* Integration le plus simple
* Au 2024/10, meteomedia ne sert plus les images de quoi porter


## Fichiers

* bin/install.sh
  * Quelques lignes pour installer les dependences
  * Voir Install pour meteo_media_school_wear
* bin/old/meteo_media_school_wear.py
  * Script python qui va mettre a jour l'image
  * URL de meteo media écrit en dure
* entities/switches/meteo_media_school_wear.yaml.disable
  * Entité utilisé pour appeler le script python quand positionné sur ON
* automations.yaml (disabled)
  * Mise a jour meteo_media_school_wear
  * Met l'entité switch sur ON toutes les 2 heures
* entities/cameras/meteo_media_school_wear.yaml.disable
  * Utilisation d'un composant camera qui va aller chercher l'image créé par le script python
* lovelace/components/meteo_media_school_wear.yaml.disable
  * Sous composant qui affiche les vetements les jours de semaines

## Solutions non retenu

* Un composant demande beaucoup de mise en oeuvre
* Un serveur web qui fourni l'image à chaque demande
  * Trop de ressources pour un petit cas
