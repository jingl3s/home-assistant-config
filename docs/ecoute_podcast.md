# Ecouter des podcast avec execution depuis home assistant

## Objectif

Écouter des podcast journalier avant noel avec un a chaque jour et voir plus avec un bouton

Découpage:
* utilisation de podfox
* configuration pour télécharger dans media utilisé par home assistant
* creation d'un script python
  * lit le fichier mp3 le plus ancien
  * Supprime episode lu
  * met a jour flux podcast et télécharge nouveaux episodes
* Creation d'un script dans home assistant config pour appeler le script
* Ajout dans l'automatisation
* Ajout d'un bouton dans le panneau de média pour pouvoir accéder rapidement

Choix:
* Non utilisation de media player de home assistant pour simplifier execution
* Evite de gérer des appel api pour executer une lecture

## Dépendances

* Home assistant
  * Extension media player vlc
* Python 3.8 car n'a pas fonctionné avec Python 3.10 the Home assistant

## Installation

* Installer podfox via meme venv que home assistant
  * `pip install --upgrade git+https://github.com/jingl3s/podfox.git`
* Creer dossier de travail s'il n'existe
  ```bash
  mkdir ~/homeassistant_local/media/podcasts
  mkdir ~/homeassistant_local/homeassistant/extras/podfox
  nano  ~/homeassistant_local/homeassistant/extras/podfox/podfox.json
  ```
* Copier le contenu suivant dans podfox.json
  ```json
{
  "podcast-directory" : "/home/hugo/homeassistant_local/media/podcasts",
  "maxnum"            : 2,
  "maxage-days"       : 10
}
  ```
* Ajout de la source avec le flux RSS
podfox import https://feed.ausha.co/b7zAOcvwJGP1 pommeapinoel -c ~/homeassistant_local/homeassistant/extras/podfox/podfox.json

* Telecharge quelques episodes
podfox download pommeapinoel -c ~/homeassistant_local/homeassistant/extras/podfox/podfox.json --rename-files

* Creer un script dans Home assistant pour lire le fichier

  ```yaml
service: media_player.play_media
data:
  media_content_id: /home/hugo/homeassistant_local/media/podcasts/{{file_to_play}}
  media_content_type: music
target:
  entity_id: media_player.vlc
  ```

* Utiliser script python pour effectuer les enchainements
/home/hugo/homeassistant_local/venv38/bin/python /home/hugo/homeassistant_local/homeassistant/extras/podfox/ecoute_podcast.py

* Ajout d'un shell_command dans home assistant configuration
`ecoute_podcast: 'nohup /home/hugo/homeassistant_local/venv38/bin/python /home/hugo/homeassistant_local/homeassistant/extras/podfox/ecoute_podcast.py > /dev/null 2>&1 &'`

* Ajout d'un sensor dans home assistant pour savoir s'il reste des mp3
```ỳaml
platform: command_line
name: ecoute_podcast_nombre_mp3
command: "ls -1 /home/hugo/homeassistant_local/media/podcasts/pommeapinoel/*.mp3|wc -l"
scan_interval: 7200
```
* Récupération d'une image du podcast depuis https://feed.ausha.co/b7zAOcvwJGP1
  * Reduction de la taille pour la rendre plus petite
  * Renommage vers pommeapinoel.jpg
  * Copy dans le dossier d'installation de Home assistant wwww

* Ajout d'un bouton conditionnel pour jouer le morceau dans lovelace en appelant la ligne de commande
lovelace/components/ecoute_podcast.yaml
Référencé: lovelace/infos.yaml

```yaml
type: conditional
conditions:
  - entity: sensor.ecoute_podcast_nombre_mp3
    state_not: '0'
card:
  type: picture
  image: /local/pommeapinoel.jpg
  tap_action:
    action: call-service
    service: shell_command.ecoute_podcast
    service_data: {}
    target: {}
  hold_action:
    action: none
  style: |
    ha-card {
      box-shadow: none;
      background: none;
      text-shadow: 1px 1px 0 #000;
      --ha-card-background: gray;
      font-size: 25px;
      height: 300px;
    }
    img {
        height:100px;
        max-width: 100px;
    }
    .entities {
      padding: 0px 0px 0px 0px  !important;
    }
```

## Nouveaux podcast 2022/04

```sh
. /home/hugo/homeassistant_local/venv38/bin/activate
podfox import  https://feeds.podcastics.com/podcastics/podcasts/rss/2521_6ca93fac7a7bfc5bcc9547de52667300.rss les_histoires_holy_owly -c ~/homeassistant_local/homeassistant/extras/podfox/podfox.json


podfox download les_histoires_holy_owly -c ~/homeassistant_local/homeassistant/extras/podfox/podfox.json

podfox feeds -c ~/homeassistant_local/homeassistant/extras/podfox/podfox.json
```

Remplacement dans pour utiliser les_histoires_holy_owly
entities/sensors/ecoute_podcast_nombre_mp3.yaml
