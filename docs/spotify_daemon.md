# Spotify integration pour jouer de la musique sur le serveur

## Objectif
* Avoir une enceinte connecté a moindre effort pour ecouter spotify


## Installation

* Installation manuelle https://github.com/Spotifyd/spotifyd#compiling-from-source
  * Rust https://rustup.rs/
    * Le 2020/11/28 car anomalie avec rust 1.48
    * https://github.com/Spotifyd/spotifyd/issues/719
    * rustup override set 1.47.0
  * Récupération du code source depuis github
  * Compilation
  `$HOME/.cargo/bin/cargo  build --release --features "pulseaudio_backend"`

## Configuration Home assistant

* Entrée booleen créé via l'interface
  * spotify_panneau
  * mdi:music
  * input_boolean.spotify_panneau
  * Utilisé avec un bouton pour affiché/masquer la zone du media player
* Cartes Lovelace utilisé pour avoir un affichage a la demande
  * Panneau dynamique qui utilise state-cards 
    * lovelace/components/panneau_dynamique.yaml
* Demarrage et arret de spotifyd a la demande (non integre)
  * archives/entities/switches/spotifyd.yaml

<img src="../images/spotify_on_demand.png" alt="Panneau dynamique pour acceder au media player" width="400"> </img>

## Test serveur

* cd target/release
* Ligne de commande
`./spotifyd --no-daemon --username TBD --password TBD`

* Lancement de spotifyd avec fichier de configuration
`./spotifyd --no-daemon --config-path ./config.cfg`

## Fichier configuration

spotifyd.conf

```
[global]
# Your Spotify account name.
username = TBD

# Your Spotify account password.
password = TBD

# The name that gets displayed under the connect tab on
# official clients. Spaces are not allowed!
device_name = netbook

# The displayed device type in Spotify clients.
# Can be unknown, computer, tablet, smartphone, speaker, tv,
# avr (Audio/Video Receiver), stb (Set-Top Box), and audiodongle.
device_type = speaker
```

## Resources

* [https://github.com/Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd)
