# Jouer une musique le matin

## Objectif
* V1 : Utiliser une playlist Spotify et la jouer les matin de semaine avec une automatisation
- V2 : Utiliser VLC local pour jouer un mp3 disponible dans le dossier de medias (alternative car Spotify mis en pause)


## Installation

* Installer composant VLC https://www.home-assistant.io/integrations/vlc/
- [Optionel] Configurer media source / browser https://www.home-assistant.io/integrations/media_source/

## Configuration Home assistant

### Spotify

* Script musique_spotify
  * Effectue plusieurs actions en utilisant [Spotify daemon player with media player](docs/spotify_daemon.md)
  * Joue une URL playlist de spotify

### VLC

* Entréé selection (input_select) créé via l'interface
  * media_musiques
  * mdi:music
  * input_select.media_musiques
  * Utilisé pour avoir la liste des morceaux MP3 disponibles
  * Liste mise a jour manuellement car peut de changements (voir section)
* Script permettant d'utiliser le input select avec VLC
  * Nom: musique_vlc
  * mdi:music-box
  * media_content_id: '/home/utilisateur/media/musique/{{states(''input_select.media_musiques'')}}'

### Musique matin et page

* Automatisation musique_matin
  * Tous les jours 7:10
  * Jour de semaine
  * Definie le niveau sonore du PC
  * Regarde l'état de spotify et appel le script selon dispo
    * script.musique_spotify
    * script.musique_vlc


* Cartes Lovelace utilisé pour pouvoir selectionner la musique et l'essayer ou activer automatisation
  * Panneau lovelace utilisé dans plusieurs pages
    * lovelace/components/musique_ordinateur.yaml

<center><img src="../images/musique_matin.png" alt="Panneau musique matin" width="400"> </img></center>

## Mise a jour input_select fichiers musiques disponible

* Ouvrir un terminal dans le dossier
* Executer la commande pour avoir les noms de fichiers
`ls -1R`
* Reconstituer le nom de fichier avec le chemin en ajoutant un -
  * composant VLC attend un chemin complet systeme au lieu d'un chemin Home assistant
* Example (utilisation de " ou ' selon si espaces dans le nom)
```
- "/home/utilisateur/musique/musique_du_matin.mp3"
- '/home/utilisateur/musique/musique_de_jeu.mp3'
- /home/utilisateur/musique/musique_motivante.mp3
```
* Aller dans Home assistant outils de développement
* Aller dans l'onglet services (utiliser le mode graphique)
* Choisir `input_select.set_options`
* Choisir l'entité `input_select.media_musiques`
* Coller dans Options la liste préparer avec les fichiers
* Clicker sur appeler le service




- "_Enfant/Electro_music_Los_minions_Papaya_Remix_2.mp3"
- "_Enfant/El Marinero Baila - Paco El Marinero-uTK_7MOFV4s.mp3"
- "_Enfant/Enfin, je vais a L'école-s3stesjJTKU.mp3"
- "_Enfant/Je m'habille pour aller dehors vid-F1hWGHSZumo.mp3"
- "_Enfant/La danse des Pingouins - France--xtPMTEPFug.mp3"
- "_Enfant/la_danse_d_igor.mp3"
- "_Enfant/Las Ruedas Del Autobus.mp3"
- "_Enfant/Le contraire de tout des Ogres de Barback and lyrics--yKQzwd7jqQ.mp3"
- "_Enfant/Les ogres de Barback - La vache enragée-u2O8se_a_Kw.mp3"
- "_Enfant/Les roues de l'autobus - Comptines avec les voitures et les camions.mp3"
- "_Enfant/Penguin Dance _ Brain Breaks _ Jack Hartmann-uf0uKmKwnKs.mp3"
- "_Enfant/Pitt ocha - Ces petits riens-m84jcIpHQK0.mp3"
- "_Enfant/SAMBALELÊ - chanson populaire du Brésil - avec sous-titres portugais-HSoAqHr5MYg.mp3"
- "_Enfant/Henri\ Dès\ chante\ -\ On\ ne\ verra\ jamais\ -\ chanson\ pour\ enfants-ArPCZZ_NMxM.webm"
- "podcast/noisette_68_gudrun_la_cane.mp3"